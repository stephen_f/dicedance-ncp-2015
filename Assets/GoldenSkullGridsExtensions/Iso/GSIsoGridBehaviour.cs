﻿namespace Gamelogic.Grids.GoldenSkull
{
	public class GSIsoGridBehaviour : GSGridBehaviour<DiamondPoint>
	{
	    public override int CalcSortingLayerOrder(DiamondPoint point)
		{
			return -(point.X + point.Y);
		}
	}
}