﻿namespace Gamelogic.Grids.GoldenSkull
{
	public class GSHexGridBehaviour : GSGridBehaviour<FlatHexPoint>
	{
	    public override int CalcSortingLayerOrder(FlatHexPoint point)
		{
			return -(2*point.Y + point.X);
		}
	}
}