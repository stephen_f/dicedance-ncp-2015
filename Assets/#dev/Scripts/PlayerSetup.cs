﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using InControl;
using UnityEngine.Networking.NetworkSystem;
using UnityEngine.UI;

public class PlayerSetup : MonoBehaviour {

    public List<InputDevice> PlayerDevices = new List<InputDevice>();

    public Image[] Images;
   

    public bool CanSetup;


    void Start ()
	{
	   
	}
	
	
	void Update ()
    {

        PlayersSetupUpdate();

    }

    private void PlayersSetupUpdate()
    {
        if(!CanSetup)
            return;


        var inputDevice = InputManager.ActiveDevice;

        if (inputDevice != null)
        {
            if (inputDevice.Action1)
            {
                PlayersAdd(inputDevice);
            }
            if (inputDevice.Action2)
            {
                PlayersRemove(inputDevice);
            }
        }

        GameManager.Instance.SavePlayerDevices(PlayerDevices);
    }

    private void PlayersRemove(InputDevice inputDevice)
    {
        if (!PlayerDevices.Contains(inputDevice))
            return;

        var index = PlayerDevices.IndexOf(inputDevice);
        OnRemove(index);

        PlayerDevices.Remove(inputDevice);
        

       
    }

    private void OnRemove(int index)
    {
        GameManager.Instance.SetPlayerCanMove(index, false);
        
        imgDisable(index);
    }


    private void PlayersAdd(InputDevice inputDevice)
    {
        if(PlayerDevices.Contains(inputDevice))
            return;

        PlayerDevices.Add(inputDevice);
       

        var index = PlayerDevices.IndexOf(inputDevice);
        OnAdd(index, inputDevice);
    }

    private void OnAdd(int index, InputDevice inputDevice)
    {
        GameManager.Instance.SetPlayerCanMove(index, true);
        GameManager.Instance.SetPlayerControlDevice(index, inputDevice);
        imgEnable(index);
    }

    private void imgEnable(int index)
    {
        Images[index].GetComponent<RectTransform>().DOMove(Vector3.up*2000, 0.2f).SetRelative();
    }

    private void imgDisable(int index)
    {
        Images[index].GetComponent<RectTransform>().DOMove(Vector3.down * 2000, 0.2f).SetRelative();
    }

}
