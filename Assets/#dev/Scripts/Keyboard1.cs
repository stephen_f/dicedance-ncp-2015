﻿using System;
using System.Collections;
using UnityEngine;
using InControl;


namespace CustomProfile
{
    // This custom profile is enabled by adding it to the Custom Profiles list
    // on the InControlManager component, or you can attach it yourself like so:
    // InputManager.AttachDevice( new UnityInputDevice( "KeyboardAndMouseProfile" ) );
    //
    public class Keyboard1 : CustomInputDeviceProfile
    {
        public Keyboard1()
        {
            Name = "Keyboard/1";
            Meta = "A keyboard 1  combination profile ";

            ButtonMappings = new[] {
                new InputControlMapping {
                    Handle = "Confirm",
                    Target = InputControlType.Action1,
                    Source = KeyCodeButton( KeyCode.Space , KeyCode.G)
                },
                new InputControlMapping {
                    Handle = "Cancel",
                    Target = InputControlType.Action2,
                    Source = KeyCodeButton( KeyCode.F ),
                }
            };

            AnalogMappings = new[] {
                new InputControlMapping {
                    Handle = "Move Up",
                    Target = InputControlType.LeftStickUp,
                    Source = KeyCodeButton( KeyCode.W )
                },
                new InputControlMapping {
                    Handle = "Move Down",
                    Target = InputControlType.LeftStickDown,
                    Source = KeyCodeButton( KeyCode.S )
                },
                new InputControlMapping {
                    Handle = "Move Left",
                    Target = InputControlType.LeftStickLeft,
                    Source = KeyCodeButton( KeyCode.A )
                },
                new InputControlMapping {
                    Handle = "Move Right",
                    Target = InputControlType.LeftStickRight,
                    Source = KeyCodeButton( KeyCode.D )
                }
            };
        }
    }
}

