﻿using System;
using System.Collections;
using UnityEngine;
using InControl;


namespace CustomProfile
{
    // This custom profile is enabled by adding it to the Custom Profiles list
    // on the InControlManager component, or you can attach it yourself like so:
    // InputManager.AttachDevice( new UnityInputDevice( "KeyboardAndMouseProfile" ) );
    //
    public class Keyboard2 : CustomInputDeviceProfile
    {
        public Keyboard2()
        {
            Name = "Keyboard/1";
            Meta = "A keyboard 1  combination profile ";

            ButtonMappings = new[] {
                new InputControlMapping {
                    Handle = "Confirm",
                    Target = InputControlType.Action1,
                    Source = KeyCodeButton( KeyCode.RightControl, KeyCode.Return )
                },
                new InputControlMapping {
                    Handle = "Cancel",
                    Target = InputControlType.Action2,
                    Source = KeyCodeButton( KeyCode.RightShift ),
                }
            };

            AnalogMappings = new[] {
                new InputControlMapping {
                    Handle = "Move Up",
                    Target = InputControlType.LeftStickUp,
                    Source = KeyCodeButton( KeyCode.UpArrow )
                },
                new InputControlMapping {
                    Handle = "Move Down",
                    Target = InputControlType.LeftStickDown,
                    Source = KeyCodeButton( KeyCode.DownArrow )
                },
                new InputControlMapping {
                    Handle = "Move Left",
                    Target = InputControlType.LeftStickLeft,
                    Source = KeyCodeButton( KeyCode.LeftArrow )
                },
                new InputControlMapping {
                    Handle = "Move Right",
                    Target = InputControlType.LeftStickRight,
                    Source = KeyCodeButton( KeyCode.RightArrow )
                }
            };
        }
    }
}

