﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using UnityEngine;
using Gamelogic;
using Gamelogic.Grids;
using Gamelogic.Grids.GoldenSkull;
using InControl;
using UnityEngine.UI;

public class DanceBehaviour : GSGridBehaviour<FlatHexPoint>
{
    public GameObject RollText;
    public Canvas RollCanvas;
    public Text LuckDiceText;

    private IGrid<DanceCell, FlatHexPoint> _danceGrid;

    private int luckBonus;
    public PlayerController LuckPlayer;

    public Vector3 winPoint;

    //from GameManager
    private PlayerController[] _players;

    public bool canRandomDigCells = true;
    public bool firstTimeSpawn = true;
    public Vector3 OffsetVeritcal = Vector3.up*100;

    //

    public override int CalcSortingLayerOrder(FlatHexPoint point)
    {
        return -(2 * point.Y + point.X);
    }

    public int LuckBonus
    {
        get { return luckBonus; }
        set
        {

            luckBonus = value;

            UpdateLuckBonus();
        }
    }



    public void Awake()
    {
        Invoke("LuckDiceAnim", 1f);

        SpawnPlayers();

        luckBonus = 1;
        //мб рефы ?
        _players = GameManager.Instance.Players;
        

        CheckFirstTimeSpawn();
    }

    private void LuckDiceAnim()
    {
        
        Sequence s = DOTween.Sequence();
        s.Append(LuckDiceText.transform.parent.DOMove(new Vector3(0f, 0f, 0f), 1f));
        s.Insert(0.9f, LuckDiceText.transform.parent.GetComponent<RectTransform>().DOScale(new Vector3(0.4f, 0.4f, 0.4f), 0.1f));
            s.SetLoops(-1, LoopType.Restart).SetRelative().SetEase(Ease.InOutCirc);

    }

    private void CheckFirstTimeSpawn()
    {
        if (firstTimeSpawn)
        {
            canRandomDigCells = false;
            foreach (var player in _players)
            {
                player.CanMove = false;
            }
        }
        
    }
    private void DoneFirstTimeSpawn()
    {
        if(!firstTimeSpawn)
            return;

        
        
        
        firstTimeSpawn = false;
    }
    private static void SpawnPlayers()
    {
        GameManager.Instance.SpawnPlayers();

       
    }

    public void Update()
    {

       
        // Debug
        if (Input.GetKeyDown(KeyCode.P))
        {
            GameManager.Instance.NextRound();
        }
    }

    public class Dice
    {
        public enum damagetype
        {
            Fail,
            Normal,
            Crit
        }

        public int Roll;
        public damagetype DamageType = damagetype.Normal;

    }






    public void Init()
    {
        _danceGrid = Grid.CastValues<DanceCell, FlatHexPoint>();

        foreach (var point in _danceGrid)
        {
            _danceGrid[point].transform.position += Vector3.back * 100;
        }


        SetRandomDiggable();
        SetLuckPlayer();


       
    }

   

    private void SetLuckPlayer()
    {
        LuckPlayer = _players.RandomItem();
    }

    private void SetRandomDiggable()
    {
        if (firstTimeSpawn)
        {
            _danceGrid[FlatHexPoint.Zero].IsWinLoot = true;
        }

        if (!canRandomDigCells)
            return;

        

        foreach (var point in _danceGrid)
        {
            //Интересное сравнение в итоге приводит инт в булеан 
            //В оригинале было почему то Random.Range(0, 2) == 1
            _danceGrid[point].IsDiggable = Random.Range(0, 2) == 1;
        }
    }


    public bool CheckIfNeighbors(FlatHexPoint hexPointPosition, FlatHexPoint anotherPoint)
    {
        var playerNeighbors = _danceGrid.GetNeighbors(hexPointPosition);

        foreach (var point in playerNeighbors)
        {
            if (anotherPoint == point)
                return true;
        }
        return false;
    }

    public FlatHexPoint GetPlayerStartPostion(PlayerController player)
    {
        


        List<FlatHexPoint> borderPoints = new List<FlatHexPoint>();
        foreach (var point in _danceGrid)
        {
            //We strore all the neighbors of a rectPoint in this list
            var pointrNeighbors = _danceGrid.GetNeighbors(point);

            if (pointrNeighbors.Count() < 4)
            {
                //if  neighbours count is less then 4 then they are corner cells
                borderPoints.Add(point);

            }
        }

        var rnd = borderPoints.RandomItem();

        while (GetPlayerAt(rnd) != null)
        {
            rnd = borderPoints.RandomItem();
        }

        //убираем dig
        _danceGrid[rnd].IsDiggable = false;

        if (firstTimeSpawn)
        {
            var tmpindex = player.Index;

            //FIX
            if (tmpindex == 0)
                tmpindex = 4;

            return borderPoints[tmpindex];
        }

        return rnd;

    }


    public void SetVisibleAround(FlatHexPoint point)
    {
        var neighbors = _danceGrid.GetNeighbors(point);

        foreach (var hex in neighbors)
        {
            var Cell = _danceGrid[hex];

            Cell.IsVisible = true;
        }

        var midleCell = _danceGrid[point];
        midleCell.IsVisible = true;
    }

    public DanceCell GetCell(FlatHexPoint point)
    {
        return _danceGrid[point];
    }
   

    public bool IsPlayerStandHere(FlatHexPoint anotherPoint)
    {
        foreach (var player in _players)
        {
            if (player.HexPointPosition == anotherPoint)
            {
                //Не реагируем на трупы
                if(player.isDead)
                    continue;

                return false;
            }
        }

        return true;
    }

    public void LuckCheckTwoSides(FlatHexPoint targetPoint, FlatHexPoint fromPoint)
    {
        var diceAttacker = new Dice();
        var diceTarget = new Dice();

        var luckBonusAttacker = GetPlayerAt(fromPoint) == LuckPlayer ? LuckBonus : 0;
        var luckBonusTarget = GetPlayerAt(targetPoint) == LuckPlayer ? LuckBonus : 0;

        diceAttacker.Roll = DicesRoll(6, 1, luckBonusAttacker);
        diceTarget.Roll = DicesRoll(6, 1, luckBonusTarget);

        iDamagable attacker = GetDamagable(fromPoint);
        iDamagable target = GetDamagable(targetPoint);

        //RollText.transform.position = Map[targetPoint];
        
        var textAttacker = CreateBattleText(diceAttacker.Roll.ToString() , Map[fromPoint] + OffsetVeritcal, 1.0f);
        var textTarget = CreateBattleText(diceTarget.Roll.ToString(), Map[targetPoint] + OffsetVeritcal, 1.0f);

        //крит ролл
        if (diceAttacker.Roll == 6)
        {
            diceAttacker.DamageType = Dice.damagetype.Crit;

        }
        if (diceTarget.Roll == 6)
        {
            diceTarget.DamageType = Dice.damagetype.Crit;
        }

        //fail roll
        if (diceAttacker.Roll == 1)
        {
            diceAttacker.DamageType = Dice.damagetype.Fail;

        }
        if (diceTarget.Roll == 1)
        {
            diceTarget.DamageType = Dice.damagetype.Fail;
        }

        //doCrit
        if (diceAttacker.DamageType == Dice.damagetype.Crit)
        {
            //doube damage
            target.ReciveCritDamage();
            textAttacker.GetComponent<Text>().DOColor(Color.green, 0.1f);
        }
        if (diceTarget.DamageType == Dice.damagetype.Crit)
        {
            //counter - attack!
            attacker.ReciveNormalDamage();
            textTarget.GetComponent<Text>().DOColor(Color.green, 0.1f);
        }
        //doFail
        if (diceAttacker.DamageType == Dice.damagetype.Fail)
        {
            //hit yourself
            textAttacker.GetComponent<Text>().DOColor(Color.red, 0.1f);
            attacker.ReciveFailDamage();
        }
        if (diceTarget.DamageType == Dice.damagetype.Fail)
        {
            //hit yourself
            target.ReciveFailDamage();
            textTarget.GetComponent<Text>().DOColor(Color.red, 0.1f);
        }
        //doNormal
        if (diceAttacker.DamageType == Dice.damagetype.Normal)
        {
            if (diceAttacker.Roll > diceTarget.Roll)
            {
                //normal attack
                target.ReciveNormalDamage();
                textAttacker.GetComponent<Text>().DOColor(Color.yellow, 0.1f);
            }
            else
            {
                //при равенсте и ничье
                //nothing
                attacker.ReciveBlock();
            }
        }

    }

    private GameObject CreateBattleText(string text, Vector3 position, float duration)
    {
        position = new Vector3(position.x + Random.Range(-50f, 50f), position.y, position.z);
        GameObject textclone = (GameObject)Instantiate(RollText, position, Quaternion.identity);
        textclone.transform.SetParent(RollCanvas.transform);
        textclone.GetComponent<Text>().text = text;

        Sequence s = DOTween.Sequence();
        s.Append(textclone.GetComponent<RectTransform>().DOMoveY(50, duration).SetEase(Ease.OutCirc)).SetRelative();
        textclone.GetComponent<RectTransform>().DOScale(Vector3.one * 1.2f, 1).SetLoops(2, LoopType.Yoyo);
        textclone.GetComponent<RectTransform>().DOShakeRotation(.5f, new Vector3(0,0,1)* 360, 10);
        


        Destroy(textclone, duration*2);



        return textclone;
    }


    public bool LuckCheck(FlatHexPoint targetPoint, FlatHexPoint fromPoint)
    {
        var diceAttacker = new Dice();
        
        var luckBonusAttacker = GetPlayerAt(fromPoint) == LuckPlayer ? LuckBonus : 0;

        diceAttacker.Roll = DicesRoll(6, 1, luckBonusAttacker);



        iDamagable attacker = GetDamagable(fromPoint);

        var textAttacker = CreateBattleText(diceAttacker.Roll.ToString(), Map[fromPoint], 1.0f);
        

        //крит ролл
        if (diceAttacker.Roll == 6)
        {
            diceAttacker.DamageType = Dice.damagetype.Crit;

        }
       

        //fail roll
        if (diceAttacker.Roll == 1)
        {
            diceAttacker.DamageType = Dice.damagetype.Fail;

        }
       

        //doCrit
        if (diceAttacker.DamageType == Dice.damagetype.Crit)
        {
            //doube damage
            textAttacker.GetComponent<Text>().DOColor(Color.green, 0.1f);

            return true;
        }
        
        //doFail
        if (diceAttacker.DamageType == Dice.damagetype.Fail)
        {
            //hit yourself
            textAttacker.GetComponent<Text>().DOColor(Color.red, 0.1f);
            attacker.ReciveFailDamage();
            return false;
        }
        
        //doNormal
        if (diceAttacker.DamageType == Dice.damagetype.Normal)
        {
           textAttacker.GetComponent<Text>().DOColor(Color.yellow, 0.1f);
            return true;
        }

        return false;
    }
    private iDamagable GetDamagable(FlatHexPoint fromPoint)
    {
        var allDamagable = FindObjectsOfInterface<iDamagable>();

        foreach (var damagable in allDamagable)
        {
            if (damagable.GetPosition() == fromPoint)
            {
                return damagable;
            }
        }
        return null;
    }


    public PlayerController GetPlayerAt(FlatHexPoint point)
    {
        foreach (var player in _players)
        {
            if (player.HexPointPosition == point)
                return player;
        }
        return null;
    }

    private int DicesRoll(int diceFace, int diceCount, int luckBonus)
    {
        int roll = 0;

        for (int d = 0; d < diceCount; d++)
        {
            roll += Random.Range(1 + luckBonus, diceFace);
        }


        return roll;
    }

    public void LuckBonusAdd(int add)
    {
        LuckBonus += add;
    }

    private void UpdateLuckBonus()
    {
        LuckDiceText.text = LuckBonus.ToString();
    }

    public void OnDeath(PlayerController player)
    {
        
        LuckBonusAdd(1);


        //Если умрает игрок с кубиком
        if (player == LuckPlayer)
        {

            DropLuckDice(player.DeadPosition);

        }
        
        //Если остался только Игрок с кубиком
        if (CountAlive() == 1)
        {
            OnRoundEnd(player);
        }

    }

    private void DropLuckDice(FlatHexPoint deadPosition)
    {
        _danceGrid[deadPosition].IsWinLoot = true;
        winPoint = Map[deadPosition];
    }

    //Вызывается из игрока, не очень правильно
    public void OnRoundEnd(PlayerController winner)
    {
        //Сохранят колво игроков после меню
        GameManager.Instance.PlayerCount = GameManager.Instance.SavedPlayerDevices.Count;


        LuckDiceText.transform.parent.DOMove(winner.transform.position, 1f);

        //Debug.Log("Winner " + winner);
        if(!firstTimeSpawn)
            GameManager.Instance.AddKills(GetPlayerNumber(winner));

        StartCoroutine(RoundEnd());
    }

    private IEnumerator RoundEnd()
    {
        //done
        DoneFirstTimeSpawn();

        

        yield return new WaitForSeconds(1f);

        if(!GameManager.Instance.isGameOver)
            GameManager.Instance.NextRound();
    }

    private int CountAlive()
    {
        int alive = 0;

        foreach (var player in _players)
        {
            if(!player.isDead)
                alive++;
        }
        return alive;
    }

    private int GetPlayerNumber(iDamagable lastAttacker)
    {
        var i = 0;
        foreach (var player in _players)
        {
            if (player == (PlayerController) lastAttacker)
            {
                return i;
            }
            i++;
        }
        //в 5ый слот сюда все рандомные убийства чем нибудь другим
        return 4;
    }

    public PlayerController GetRandomAlivePlayer(PlayerController execptMe)
    {
        List<PlayerController> alviePlayers = new List<PlayerController>();

        foreach (var player in _players)
        {
            if(player.isDead)
               continue;
            if(execptMe == player)
               continue;

            alviePlayers.Add(player);
        }

        if (alviePlayers.Count > 0)
        {
            return alviePlayers.RandomItem();
        }
        else
        {
            return null;
        }
        
    }


    
}
