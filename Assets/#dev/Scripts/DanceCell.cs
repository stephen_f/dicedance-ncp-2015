﻿using UnityEngine;
using System.Collections;
using Gamelogic.Grids;
using Gamelogic.Grids.GoldenSkull;

public class DanceCell : GSCell
{


    //Keep states private
    private bool isVisible;
    private bool isDiggable;
    private bool isWinLoot;

    public bool isWalkable = true;
    public bool isTarget;

    private Color targetColor;
    private bool isBeat = false;

    //Provide public properties to change the state
    public bool IsVisible
    {
        get { return isVisible; }
        set
        {
            isVisible = value;
            UpdatePresentation(); //Update presentation to display new state.
        }
    }

    public bool IsDiggable
    {
        get { return isDiggable; }
        set
        {
            isDiggable = value;
            UpdatePresentation();
        }
    }

    public bool IsWinLoot
    {
        get { return isWinLoot; }
        set
        {
            isWinLoot = value;
            UpdatePresentation();
        }
    }

    public bool IsTarget
    {
        get { return isTarget; }
        set
        {
            isTarget = value;
            UpdatePresentation();
        }
    }


    private void UpdatePresentation()
    {
        //logic to change cells appearance based on two states

        targetColor = Color.cyan;


        if (isVisible)
            Color = Color.white;

        if (IsDiggable)
            Color = Color.yellow;

        if (!isVisible)
            Color = Color.black;

        if (isWinLoot)
            Color = Color.red;

        if (isTarget)
        {
            if(isBeat)
                Color = targetColor;
        }

         if(!isWalkable)  
            Color = new Color(1.0f, 1.0f, 1.0f, 0.2f);



    }

    public void Start()
    {
        InvokeRepeating("OnBeat",1f,1f);
        InvokeRepeating("OnBeat2",0.5f,1f);
    }

    public void LateUpdate()
    {
        isTarget = false;
    }

    void OnBeat()
    {
        isBeat = true;

    }
    void OnBeat2()
    {
        isBeat = false;
    }

    public void Update()
    {
        UpdatePresentation();
    }

    public void OnClicked()
    {
        //nothing
    }

}
