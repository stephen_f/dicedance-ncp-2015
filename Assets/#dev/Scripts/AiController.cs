﻿using UnityEngine;
using System.Collections;

public class AiController : MonoBehaviour
{
    private PlayerController _player;

    private DanceBehaviour _danceBehaviour;
    private PlayerController targetPlayer;

    private bool _isSearchPoint = false;
    private bool _isSearchTarget = false;

    public void Start()
    {
        _isSearchTarget = true;
        _player = GetComponent<PlayerController>();
        _danceBehaviour = _player.DanceBehaviour;
        SetNewTargetPlayer();

        InvokeRepeating("RhythmEveryBeat", _player.BeatEvery * 2, _player.BeatEvery * 2);

    }

    private void SetNewTargetPlayer()
    {
        targetPlayer = _danceBehaviour.GetRandomAlivePlayer(_player);

        if (targetPlayer == null)
        {
            //Если цели закончились
            _isSearchTarget = false;
         }
        
    }

    private void RhythmEveryBeat()
    {
        if(_player.isSkipTurn)
            return;

        //Переключение стейтов когда появлятеся дроп кубиков
        if (_danceBehaviour.LuckPlayer.isDead)
        {
            _isSearchTarget = false;
            _isSearchPoint = true;
        }

        //n% шанс поменять цель
        var rndMove = Random.Range(0, 10);
            if(rndMove < 3)
                SetNewTargetPlayer();


        if (_isSearchPoint)
        {
           var targetVector = _player.DanceBehaviour.winPoint;
            _player.DirectionMove(targetVector - transform.position);
        }

        if (_isSearchTarget)
        {
            if (targetPlayer.isDead)
                SetNewTargetPlayer();

            _player.DirectionMove(targetPlayer.transform.position - transform.position);
        }
        

    }

    
}
