﻿using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using Gamelogic.Grids;
using InControl;
using System.Collections;
using UnityEngine.UI;
using Random = UnityEngine.Random;



public class PlayerController : MonoBehaviour, iDamagable {

    public FlatHexPoint HexPointPosition;
    public FlatHexPoint DeadPosition;
    public DanceBehaviour DanceBehaviour;
    private int _health = 3;

    public bool CanMove = true;
    private bool CanInputBeat = false;
    public float inputWindow_half = 0.1f;
    public float BeatEvery = 1f;

    public InputDevice inputDevice;
    public bool isDead = false;
    public bool isAi = false;
    public int Index;
    private bool CanBeatMiss = false;
    private bool isBeatMissed;

    public Slider HpSlider;

    private int currnetTurn;

    public Queue<PlayerController> BattleQueue = new Queue<PlayerController>();
    public bool isSkipTurn = false;

    public AudioClip SoundFailDice;
    public AudioClip SoundSuccessDig;
    public AudioClip SoundCritDice;
    public AudioClip SoundNormalDice;
    public AudioClip SoundBlock;
    public Animator Arrow;


    public int Health
    {
        get { return _health; }
        set
        {
            _health = value;
            HpSlider.value = value;
            if (_health <= 0)
            {
                Death();
            }
        }
    }

    private void Death()
    {
        if(isDead)
            return;

        StopAllCoroutines();
        CancelInvoke();

        isDead = true;
        DeadPosition = HexPointPosition;


        //diableCanvas with hp
        HpSlider.transform.parent.gameObject.SetActive(false);
        GetComponentInChildren<SpriteRenderer>().enabled = false;

        DanceBehaviour.OnDeath(this);
        
    }

    public void Start()
    {
        //init
        DanceBehaviour = FindObjectOfType<DanceBehaviour>();
        HpSlider.value = Health;


        Invoke("StartPos", 0.1f);
        

        InvokeRepeating("RhythmEveryBeat", BeatEvery, BeatEvery);
        InvokeRepeating("RhythmEveryBeat_min", BeatEvery - inputWindow_half, BeatEvery);
        InvokeRepeating("RhythmEveryBeat_max", BeatEvery + inputWindow_half, BeatEvery);

        
    }

    public void Update()
    {
        inputDevice = GameManager.Instance.GetPlayerInputDevice(Index);

        RhythmInputUpdate();
    }

    void StartPos()
    {
        MoveTo(DanceBehaviour.GetPlayerStartPostion(this));
    }
    

    private void RhythmInputUpdate()
    {
        Arrow.SetFloat("x", 0);
        Arrow.SetFloat("y", 0);

        if (isDead)
            return;
        if(isAi)
            return;
        if(!CanMove)
            return;
        if (isSkipTurn)
            return;


        if (inputDevice.Direction.Vector.magnitude > 0.2f)
        {
            DrawVector(inputDevice.Direction.Vector);

            if(isBeatMissed)
                return;

            if (inputDevice.Action1.WasPressed)
            {
               

                if (CanInputBeat)
                    DirectionMove(inputDevice.Direction.Vector);
                if (CanBeatMiss)
                    OnBeatMiss();
            }
        }
    }

    private void OnBeatMiss()
    {
        isBeatMissed = true;
    }

    private void RhythmEveryBeat_min()
    {
        if (isSkipTurn)
            return;

        CanBeatMiss = false;

        CanInputBeat = true;
        GetComponentInChildren<SpriteRenderer>().color = Color.green;
        
    }
    private void RhythmEveryBeat_max()
    {
        if (isSkipTurn)
            return;

        CanBeatMiss = false;
        CanInputBeat = false;
        GetComponentInChildren<SpriteRenderer>().color = Color.white;

        isBeatMissed = false;
        CanBeatMiss = true;


        CheckBattle();


        currnetTurn++;

    }

    private void CheckBattle()
    {
        if (BattleQueue.Count > 0)
        {
            BattleQueue.Dequeue();
            ReciveNormalDamage();
            

        }

    }

    private void RhythmEveryBeat()
    {
        if (isSkipTurn)
            return;

    }



    private void DrawVector(Vector2 vector)
    {
        

        Arrow.SetFloat("x",vector.x);
        Arrow.SetFloat("y",vector.y);
        Debug.DrawRay(transform.position, vector * 100f, Color.blue, 0.01f);

        var mainDir = FlatHexPoint.MainDirections;
        var maxDotProduct = 0f;
        FlatHexPoint maxCloseDirection = new FlatHexPoint();

        //клетка а которй стою
        DanceBehaviour.GetCell(HexPointPosition).IsTarget = true;

        
        foreach (var direction in mainDir)
        {
            var tmpdot = Vector3.Dot(vector, DanceBehaviour.Map[direction]);

            if (tmpdot > maxDotProduct)
            {
                maxCloseDirection = direction;
                maxDotProduct = tmpdot;
            }
        }

        var anotherPoint = HexPointPosition + maxCloseDirection;


        bool isNeighbors = DanceBehaviour.CheckIfNeighbors(HexPointPosition, anotherPoint);

        
        if (isNeighbors)
        {
            

            var cell = DanceBehaviour.GetCell(anotherPoint);
            cell.IsTarget = true;

        }
        else
        {
            //выход за пределы поля
        }

        
    }

    /// <summary>
    /// Сравнивает все направления и вектор из геймпада
    /// </summary>
    /// <param name="inputDirection"></param>
    public void DirectionMove(Vector2 inputDirection)
    {
        var mainDir = FlatHexPoint.MainDirections;
        var maxDotProduct = 0f;
        FlatHexPoint maxCloseDirection = new FlatHexPoint();

        
        foreach (var direction in mainDir)
        {
            var tmpdot = Vector3.Dot(inputDirection, DanceBehaviour.Map[direction]);

            if (tmpdot > maxDotProduct)
            {
                maxCloseDirection = direction;
                maxDotProduct = tmpdot;
            }
        }

        var anotherPoint = HexPointPosition + maxCloseDirection;


        bool isNeighbors = DanceBehaviour.CheckIfNeighbors(HexPointPosition, anotherPoint);
        

        if (isNeighbors)
        {
            //эти anotherPoint, внутри поля
            var cell = DanceBehaviour.GetCell(anotherPoint);
           
            if(!cell.isWalkable)
                return;

            if (!cell.IsDiggable)
            {
                if (cell.IsWinLoot)
                {
                    //Подбриаем дайсы
                    DanceBehaviour.OnRoundEnd(this);
                }

                //Ести ли другой игрок
                if (!DanceBehaviour.IsPlayerStandHere(anotherPoint))
                {
                    AttackaAnim(anotherPoint);
                    
                    //Атака игрока
                    //Цель скипает ход
                    DanceBehaviour.GetPlayerAt(anotherPoint).SkipTurns(1);
                    DanceBehaviour.LuckCheckTwoSides(anotherPoint, HexPointPosition);

                    GameManager.Instance.BattleSoundAdd(2f);
                    //Атакующтй скпиет 2 хода
                    SkipTurns(2);
                   

                    return;
                }
                MoveTo(anotherPoint);

            }
            else
            {
                if (DanceBehaviour.LuckCheck(anotherPoint, HexPointPosition))
                {
                    Dig(cell);
                }
                
                
            }
        }
        else
        {
            //выход за пределы поля
        }


    }

    private void AttackaAnim(FlatHexPoint anotherPoint)
    {
        Sequence s = DOTween.Sequence();
        // Add an move tween that will last the whole Sequence's duration
        s.Append(
            transform.DOMove(DanceBehaviour.GetCell(anotherPoint).transform.position + DanceBehaviour.OffsetVeritcal, 1f)
                .SetEase(Ease.OutCirc));
        s.SetLoops(2, LoopType.Yoyo);
    }

    private void SkipTurns(int count)
    {
        StartCoroutine(Skiproutine(count));
    }

    private IEnumerator Skiproutine(int count)
    {
        isSkipTurn = true;
        yield return new WaitForSeconds(count);
        isSkipTurn = false;
    }


    private void Dig(DanceCell cell)
    {
        var animDuration = 0.4f;

        Sequence s = DOTween.Sequence();
        s.Append(transform.DOMove(cell.transform.position + DanceBehaviour.OffsetVeritcal, animDuration).SetEase(Ease.InOutBack));
        s.SetLoops(2, LoopType.Yoyo);

        StartCoroutine(DigState(cell, animDuration));

        PlayClip(SoundSuccessDig,true);

    }

    private void PlayClip(AudioClip clip, bool isPitch)
    {
        var audio = GetComponent<AudioSource>();
        audio.clip = clip;
        if(isPitch)
            audio.pitch = Random.Range(0.9f, 1.1f);
        else
        {
            audio.pitch = 1f;
        }
        audio.Play();

    }

    private IEnumerator DigState(DanceCell cell, float animDuration)
    {
        yield return new WaitForSeconds(animDuration);
        //копаем
        cell.IsDiggable = false;
    }


    /// <summary>
    /// Передвигает игрока к точке
    /// </summary>
    /// <param name="point"></param>
    private void MoveTo(FlatHexPoint point)
    {

        transform.DOMove(DanceBehaviour.Map[point] + DanceBehaviour.OffsetVeritcal, 1f);
        HexPointPosition = point;
        GetComponentInChildren<SpriteRenderer>().sortingOrder = DanceBehaviour.CalcSortingLayerOrder(point) + 1;

        DanceBehaviour.SetVisibleAround(point);

    }


    public FlatHexPoint GetPosition()
    {
        return HexPointPosition;
    }

    public void ReciveNormalDamage()
    {
        Health--;

        PlayClip(SoundNormalDice, true);

    }

    public void ReciveCritDamage()
    {
        Health -= 2;
        PlayClip(SoundCritDice, true);
    }

    public void ReciveFailDamage()
    {

        Health--;
        PlayClip(SoundFailDice, true);

        HitColorFeedback(0.1f);

    }


    public void ReciveBlock()
    {
        PlayClip(SoundBlock, true);
    }
    private void HitColorFeedback(float animDuration)
    {
        Sequence s = DOTween.Sequence();
        s.Append(GetComponent<SpriteRenderer>().DOColor(Color.red, animDuration).SetEase(Ease.InOutBack));
        s.SetLoops(2, LoopType.Yoyo);
    }
}

public interface iDamagable
{
    FlatHexPoint GetPosition();
    void ReciveNormalDamage();
    void ReciveCritDamage();
    void ReciveFailDamage();
    void ReciveBlock();
}
