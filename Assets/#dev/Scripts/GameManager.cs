﻿using System.Collections;
using UnityEngine;
using System.Collections.Generic;
using InControl;
using UnityEngine.Audio;
using UnityEngine.UI;


/// <summary>
/// The game manager is a persistent singleton that handles points and time
/// </summary>
public class GameManager : PersistentSingleton<GameManager>
{
    /// the current number of game points
    public int Points { get; private set; }

    public int[] Kills = new int[4];

    /// the current time scale
    public float TimeScale { get; private set; }
    /// true if the game is currently paused
    public bool Paused { get; set; }

    private float battleTimer;

    public PlayerController[] Players = new PlayerController[4];
    public Sprite[] PlayerSprite = new Sprite[4];
    public GameObject PlayerPrefab;

    public AudioMixerSnapshot outCombat;
    public AudioMixerSnapshot inCombat;
    public Canvas WinCanvas;
    public Image Winimage;
    public Text WinName;
    public Text WinRestart;

    [Space(10)]
    [Header("Settings")]
    public int PlayerCount = 1;
    public int MaxKillToEnd = 3;

    [Space(10)]
    [Header("Debug")]
    public bool DevLevelTest;
    // storage
    private float _savedTimeScale;
    public List<InputDevice> SavedPlayerDevices = new List<InputDevice>();
    public bool isGameOver = false;

    /// <summary>
	/// this method resets the whole game manager
	/// </summary>
	public void Reset()
    {
        Points = 0;
        TimeScale = 1f;
        Paused = false;

    }

    public void Update()
    {


        if (battleTimer > 0)
        {

            battleTimer -= Time.deltaTime;

            

        }
        if (battleTimer <= 0)
        {
            outCombat.TransitionTo(1);
        }
    }

    public void SpawnPlayers()
    {
        PlayerCount = Mathf.Clamp(PlayerCount, 1, 4);
        for (int i = 0; i < 4; i++)
        {
            var clone = Instantiate(PlayerPrefab, transform.position, transform.rotation) as GameObject;
            Players[i] = clone.GetComponent<PlayerController>();
            Players[i].gameObject.name = "Player " + i;
            

            if (i > PlayerCount - 1)
            {
                //добвляем ботов
                Players[i].gameObject.AddComponent<AiController>();
                Players[i].isAi = true;
                Players[i].gameObject.name = "Bot " + i;
            }

            Players[i].GetComponentInChildren<SpriteRenderer>().sprite = PlayerSprite[i];
            Players[i].Index = i;
        }



    }

    public void SetPlayerCanMove(int index, bool IsCanMove)
    {
        Players[index].CanMove = IsCanMove;
    }

    public void AddKills(int playerNumber)
    {
        Kills[playerNumber]++;

        CheckKillsToEnd();
    }

    private void CheckKillsToEnd()
    {
        var i = 0;
        foreach (var playerkills in Kills)
        {
            
            if (playerkills >= MaxKillToEnd)
            {
                GameOver(i);
            }
            i++;
        }
    }

    private void GameOver(int indexWinner)
    {
        isGameOver = true;
        var win = Instantiate(WinCanvas, transform.position, Quaternion.identity) as GameObject;
        WinName.text = Players[indexWinner].name;
        Winimage.sprite = PlayerSprite[indexWinner];
        StartCoroutine(RestartGame());

    }

    private IEnumerator RestartGame()
    {
        WinRestart.text = "5";
        yield return new WaitForSeconds(5f);

        MaxKillToEnd = 5;
        NextRound();


    }

    public int GetKills(int playerNumber)
    {
        return Kills[playerNumber];
    }


    ///  <summary>
    /// Adds the points in parameters to the current game points.
    /// </summary>
    /// <param name="pointsToAdd">Points to add.</param>
    public void AddPoints(int pointsToAdd)
    {
        Points += pointsToAdd;

    }

    /// <summary>
    /// use this to set the current points to the one you pass as a parameter
    /// </summary>
    /// <param name="points">Points.</param>
    public void SetPoints(int points)
    {
        Points = points;

    }

    /// <summary>
    /// sets the timescale to the one in parameters
    /// </summary>
    /// <param name="newTimeScale">New time scale.</param>
    public void SetTimeScale(float newTimeScale)
    {
        _savedTimeScale = Time.timeScale;
        Time.timeScale = newTimeScale;
    }

    /// <summary>
    /// Resets the time scale to the last saved time scale.
    /// </summary>
    public void ResetTimeScale()
    {
        Time.timeScale = _savedTimeScale;
    }

    /// <summary>
    /// Pauses the game
    /// </summary>
    public void Pause()
    {
        // if time is not already stopped		
        if (Time.timeScale > 0.0f)
        {
            Instance.SetTimeScale(0.0f);
            Instance.Paused = true;

        }
        else
        {
            Instance.ResetTimeScale();
            Instance.Paused = false;

        }
    }


    public void NextRound()
    {
        battleTimer = 0f;




        if (Application.loadedLevel != 0)
            Application.LoadLevel(Random.Range(2, Application.levelCount));
        else
        {
            Application.LoadLevel(1);
        }
    }


    public void SetPlayerControlDevice(int index, InputDevice inputDevice)
    {
        Players[index].inputDevice = inputDevice;
    }

    public void SavePlayerDevices(List<InputDevice> playerDevices)
    {
        SavedPlayerDevices = playerDevices;
    }


    public InputDevice GetPlayerInputDevice(int index)
    {
        if (DevLevelTest)
            return InputManager.ActiveDevice;

        if (index >= SavedPlayerDevices.Count)
        {
            return InputDevice.Null;
        }
        else
        {
            return SavedPlayerDevices[index];
        }
    }

    public void BattleSoundAdd(float time)
    {
        if (battleTimer <= 0)
        {
            inCombat.TransitionTo(1f);
        }


        battleTimer += time;
    }


}
